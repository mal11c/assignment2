
<!-- Megan Lattanzi - mal11c	
		lis5367 Assignment 2 -->

<html><head>
<title> BEST - PHP and Processing</title>
</head>


<body>


<?php

require_once './classes.php';

/* that's the end of our "model" -- our OOP. Everything from here forward is "procedural," but should hopefully be ridiculously easy to understand. 
 * 
 * Let's get the form data. */


	$first = $_POST['first'];
	$last = $_POST['last'];	
	$age = $_POST['age'];
	$income = $_POST['income'];
	
	
	
 /* now instantiate and populate the current subject */

$currsub = new subject();

$currsub->setFirstname($first);
$currsub->setLastname($last);
$currsub->setAge($age);
$currsub->setIncome($income);


/* now lets print our output */

echo "<h1> form out </h1>";
echo "Hello there, " . $currsub->getFirstname();
echo "<br> Oops I mean " . $currsub->getSpecialLastname();
echo "<br> Your code is " . $currsub->agetocolor() . " " . $currsub->incometofood();


/* NOW LETS SAVE THE RAW DATA TO DISK. */

$storage_file = "./storage.txt";

$file_handle = fopen($storage_file, 'a') or die("<br>can't open file");  

$fullstring = $currsub->getFirstname() . "," . $currsub->getLastname()  . "," . $currsub->getAge() . "," . $currsub->getIncome() . "\n";

fwrite($file_handle, $fullstring);


fclose($file_handle);

?>



