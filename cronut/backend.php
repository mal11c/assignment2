<?php

/* Megan Lattanzi - mal11c	
		lis5367 Assignment 2 */
 


require_once('./classes.php');

$storage_file = "./storage.txt";

unset($file_handle);



$csvfile = fopen($storage_file,'r');

while(!feof($csvfile)) {

	    $allfields[] = fgetcsv($csvfile);
	       
}

$subject_array = array();

foreach ($allfields as $row) {
	$currsub = new subject();

	$currsub->setFirstname($row[0]);
	$currsub->setLastname($row[1]);
	$currsub->setAge($row[2]);
	$currsub->setIncome($row[3]);

	$subject_array[] = $currsub;
}

$record_number=1;

echo "<h1>Database Records</h1>";

foreach ($subject_array as $currsub) {

	echo "<h2>Record number $record_number </h2>";
	echo "Subject name: " ;
	echo $currsub->getFirstname();
	echo " ";
	echo $currsub->getLastname();
	echo "<br>";
	echo "Subject code: " . $currsub->agetocolor() . " " . $currsub->incometofood();
	
	$record_number++;
}